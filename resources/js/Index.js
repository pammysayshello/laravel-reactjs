import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import createBrowserHistory from 'history/createBrowserHistory';
import { Router, Route } from "react-router-dom";

const history = createBrowserHistory();

/** elements */
import Header from './components/elements/Header';
import Footer from './components/elements/Footer';

/** components */
import Torrents from './components/Torrents';
import Categories from './components/Categories';

export default class Index extends Component
{
	render()
	{
		return (
			<Router history={history}>
				<div id="main">
					<Header />

					<section className="content">
						<section id="pageRight">
							<Route exact path={"/"} component={Torrents} />
							<Route exact path={"/categories"} component={Categories} />
						</section>
					</section>

					<Footer />
				</div>
			</Router>
		);
	}
}

if (document.getElementById('layout'))
	ReactDOM.render(<Index />, document.getElementById('layout'));