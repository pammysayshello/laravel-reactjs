import React, { Component } from 'react';
import { BrowserRouter as Router, NavLink } from 'react-router-dom';
import Axios from 'axios';

export default class Torrents extends Component
{
	constructor()
	{
		super();

		this.state = {
			torrents: []
		};
	}

	componentWillMount()
	{
		Axios
			.get("http://localhost:8000/api/torrents")
			.then(response => {
				this.setState({
					torrents: response.data
				})
			});
	}

	render()
	{
		return (
			<div className="torrents-index">
				<Router>
					<ul className="breadcrumbs">
						<li><NavLink to={'/'}><i className="fa fa-home"></i></NavLink></li>
						<li className="active">Torrents</li>
					</ul>
				</Router>

				<div className="page-head">
					<h3 className="heading">Torrents</h3>
					<div className="options">
						<NavLink to={'/torrent-add'} className="buttons tiny icon-right dark"><i className="fa fa-plus top0"></i> Torrent</NavLink>
					</div>
				</div>

				<div className="page-body">
					<div className="page-info">
						<div className="box dark">
							<div className="tables">
								<table>
									<thead>
										<tr>
											<th className="text-right" width="30">#</th>
											<th width="210">Category</th>
											<th className="text-right" width="60">Year</th>
											<th>Torrent</th>
											<th width="200">Created</th>
											<th width="200">Updated</th>
											<th width="160" className="textRight">Actions</th>
										</tr>
									</thead>
									<tbody>
										{
											this.state.torrents.map((torrent, index) => <tr key={index}>
												<td className="hidden-600 text-right">{ index + 1 }</td>
												<td data-content="Category">{ torrent.category }</td>
												<td className="text-right" data-content="Year">{torrent.year}</td>
												<td data-content="Torrent">{torrent.title}</td>
												<td data-content="Created">{torrent.created_on}</td>
												<td data-content="Updated">{torrent.modified_on}</td>
												<td data-content="Actions" className="actions textRight">
													<a href="http://localhost/Php/Projects/Others/001-Torrents/?token=be95b4309ef1125b10ee9f340060f0fe920fc2a8/N" className="buttons tiny red"><i className="fa fa-thumbs-down">&nbsp;</i>Inactive</a>
													<a href="http://localhost/Php/Projects/Others/001-Torrents/torrents-form.php?token=be95b4309ef1125b10ee9f340060f0fe920fc2a8" className="buttons tiny green ml5"><i className="fa fa-edit">&nbsp;</i>Edit</a>
												</td>
											</tr>)
										}
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}