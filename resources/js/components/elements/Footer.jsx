import React, { Component } from 'react';

export default class Header extends Component
{
	render()
	{
		return (
			<footer id="footer">
				<span className="copy-right">&copy; All right reserved.</span>
				<span className="markup-canvas">Developed and maintained by <a target="_blank" href="http://www.markupcanvas.com">MarkupCanvas.com</a>.</span>
				<span className="best-view">Best view: Mozilla Firefox 50+, Google Chrome 60+, Opera 45+, MSIE 10+</span>
			</footer>
		);
	}
}