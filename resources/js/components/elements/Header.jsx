import React, { Component } from 'react';
import { NavLink } from "react-router-dom";

export default class Header extends Component
{
	render()
	{
		return (
			<header id="header">
				<div className="logo">
					<img src="images/MarkupCanvas.svg" className="svg" alt="MarkupCanvas Logo" />
				</div>

				<nav id="navs">
					<ul>
						<li><NavLink exact activeClassName="active" to={"/"}><i className="fa fa-magnet"></i><span className="hidden-480">Torrents</span></NavLink></li>
						<li><NavLink exact activeClassName="active" to={"/categories"}><i className="fa fa-list-alt"></i><span className="hidden-480">Categories</span></NavLink></li>
					</ul>
				</nav>
			</header>
		);
	}
}