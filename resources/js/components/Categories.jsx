import React, { Component } from 'react';
import { BrowserRouter as Router, NavLink } from 'react-router-dom';

export default class Categories extends Component
{
	render()
	{
		return (
			<div className="torrents-index">
				<Router>
					<ul className="breadcrumbs">
						<li><NavLink to={'/'}><i className="fa fa-home"></i></NavLink></li>
						<li className="active">Categories</li>
					</ul>
				</Router>

				<div className="page-head">
					<h3 className="heading">Categories</h3>
					<div className="options">
						<NavLink to={'/categories-add'} className="buttons tiny icon-right dark"><i className="fa fa-plus top0"></i> Category</NavLink>
					</div>
				</div>
			</div>
		);
	}
}