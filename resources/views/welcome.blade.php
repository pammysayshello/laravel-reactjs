<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="/css/font-awesome.css" />
	<link rel="stylesheet" type="text/css" href="/css/admin-template.css" />
	<link rel="stylesheet" type="text/css" href="/css/admin-media-queries.css" />
	<title>Laravel-ReactJS</title>
</head>
<body>
	<div id="layout"></div>
	<script type="text/javascript" src="/js/app.js"></script>
	<script type="text/javascript" src="/js/functions.js"></script>
</body>
</html>