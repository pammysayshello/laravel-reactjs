<?php
	namespace App;

	use Illuminate\Database\Eloquent\Model;

	use DB;

	use App\Helpers\Common;

	class Torrents extends Model
	{
		protected $table = "torrents";

		private $timestamp = false;

		public function categories()
		{
			return $this->belongsTo(Categories::class);
		}

		/**
		 * @Author         : Parminder Singh
		 * @Last modified  : 12, February 2019
		 *
		 * @Project        : Laravel-ReactJS
		 * @Function name  : _grid
		 * @Description    : funciton to fetch the data from the table
		 * @Parameters     : $status as string(default 'Y')
		 *
		 * @Method         :
		 * @Returns        : raw data
		 * @Return type    : array
		 */
		public function _grid($status = Common::STATUS_ACTIVE)
		{
			# models
			$modelSelf = new self();

			# fetching data
			$arraySelf = $modelSelf->where([
				"torrents.status" => $status
			])->select([
				"categories.title as category",

				"torrents.token",
				"torrents.title",

				DB::raw("_isNull(torrents.year) AS year"),
				DB::raw("_isNull(torrents.description) AS description"),

				DB::raw("_unixToDate(torrents.created_on, '') AS created_on"),
				DB::raw("_unixToDate(torrents.modified_on, '') AS modified_on")
			])
			->leftJoin("categories", "categories.id", "=", "torrents.categories_id")
			->orderBy("categories.title", "ASC")
			->orderBy("torrents.title", "ASC")
			->orderBy("torrents.year", "ASC")
			->get()
			->toArray();

			# returning data
			return $arraySelf;
		}
	}
?>