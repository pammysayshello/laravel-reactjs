<?php
	namespace App;

	use Illuminate\Database\Eloquent\Model;

	use App\Helpers\Common;

	class Categories extends Model
	{
		protected $table = 'categories';

		private $timestamp = false;

		public function _grid($status = Common::STATUS_ACTIVE)
		{
			# models
			$modelSelf = new self();

			# fetching data
			$arraySelf = $modelSelf->where([
				"status" => $status
			])->select([
				"token",
				"title",
				"description",
				"created_on",
				"modified_on"
			])
			->orderBy("title", "ASC")
			->get()
			->toArray();

			# returning data
			return $arraySelf;
		}
	}
?>