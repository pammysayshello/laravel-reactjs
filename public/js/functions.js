;$(function() {
	window.sidebarCollapse = $('.content').hasClass("sidebar-collapse");

	// function to slider up/down elements
	$('.toggler').off("click").on("click", function() {
		var $button = $(this);
		var $target = $(this).next('ul');
		var $data = [];

		if(!$button.hasClass('wait') && !$button.parent().hasClass('active') || window.sidebarCollapse === true)
		{
			/*setting defaults*/
			$data.active = ($data.active !== undefined) ? $data.active : 'active';

			/*reading all data attributes*/
			for(var i in $button.data())
			{
				$data[i] = $button.data()[i];
			}

			/*checking active sub menu*/
			if($(".hasMenu").hasClass($data.active))
			{
				var $activeMenu = $(".hasMenu."+$data.active);
				var $activeButton = $activeMenu.children(".toggler");
				var $activeUl = $activeMenu.children(".subMenu");

				$activeUl.slideUp(200, function() {
					$activeMenu.add($activeButton).removeClass($data.active);
					$activeUl.removeAttr("style");
				}).removeClass($data.active);

				if(!$button.hasClass($data.active))
				{
					$button.addClass('wait');

					$($target).addClass($data.active).slideDown(200, function() {
						$button.removeClass('wait').addClass($data.active);
						$button.parent('.hasMenu').addClass($data.active);
					});
				}
			}
			else
			{
				$button.addClass('wait');

				$($target).addClass($data.active).slideDown(200, function() {
					$button.removeClass('wait').addClass($data.active);
					$button.parent('.hasMenu').addClass($data.active);
				});
			}
		}
	});

	$(".sidebar-toggler").off("click").on("click", function(e) {
		var $sideBar = $(this);
		var $content = $('.content');
		var $pageLeft = $('#pageLeft');
		var $pageRight = $('#pageRight');

		if($content.hasClass("sidebar-collapse"))
		{
			window.sidebarCollapse = false;

			$content.removeClass("sidebar-collapse");
		}
		else
		{
			window.sidebarCollapse = true;

			$content.addClass("sidebar-collapse");
		}

		return false;
	});

	$(".js-sidebar-floating").off("click").on("click", function(e) {
		var $body = $('body');
		var $sideBar = $(this);
		var $main = $('#main');

		if($main.hasClass("sidebar-floating"))
		{
			$main.removeClass("sidebar-floating");
		}
		else
		{
			$main.addClass("sidebar-floating");
		}

		ajax_actions($sideBar.children('a'));

		return false;
	});

	$('.mobileMenu').off("click").on("click", function() {
		var $this = $(this);
		var $target = $('#pageLeft');

		if($target.hasClass('wait')) return false;
		if($target.hasClass('active'))
		{
			$target.slideUp(100, function() {
				$target.removeClass('active wait').removeAttr("style");
				$this.removeClass('active');
			}).addClass('wait');
		}
		else
		{
			$this.addClass('active');
			$target.slideDown(100, function() {
				$target.removeClass('wait');
			}).addClass('active wait');
		}
	});

	$('img.svg').each(function(){
		var $img = $(this);
		var imgID = $img.attr('id');
		var imgClass = $img.attr('class');
		var imgURL = $img.attr('src');

		$.get(imgURL, function(data) {
			// Get the SVG tag, ignore the rest
			var $svg = $(data).find('svg');

			$svg.find('path').removeAttr('fill');

			// Add replaced image's ID to the new SVG
			if(typeof imgID !== 'undefined') {
				$svg = $svg.attr('id', imgID);
			}
			// Add replaced image's classes to the new SVG
			if(typeof imgClass !== 'undefined') {
				$svg = $svg.attr('class', imgClass + ' replaced-svg');
			}

			// Remove any invalid XML tags as per http://validator.w3.org
			$svg = $svg.removeAttr('xmlns:a');

			// Check if the viewport is set, if the viewport is not set the SVG wont't scale.
			if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
				$svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'));
			}

			// Replace image with new SVG
			$img.replaceWith($svg);
		}, 'xml');
	});

	$(document).off('click', '[data-toggle]').on('click', '[data-toggle]', function(e) {
		var $this = $(this);
		var $class = $this.data('class') || '';
		var $target = $($this.data('toggle'));

		$target[($target.is(":hidden")) ? 'slideDown' : 'slideUp'](150, function(e) {
			if($class.selfClass !== undefined) $this.toggleClass($class.selfClass);
			if($class.parent !== undefined) $this.parent().toggleClass($class.parent);
			if($class.target !== undefined) $target[($target.hasClass($class.target) ? 'removeClass' : 'addClass')]($class.target).removeAttr('style');
		});
	});
});

$(window).on('load', function() {
	$(window).resize();
});

$(window).resize(function(event) {
	if(this.resizeTo) clearTimeout(this.resizeTo);
	this.resizeTo = setTimeout(function() {
		$(this).trigger('resizeEnd');
	}, 20);
});

$(window).bind('resizeEnd', function(event) {
	var width = $(window).outerWidth(true);

	event.stopImmediatePropagation();
});

/*functions*/
function redirect(location) {
	if(confirm('Sure..?') === true)
	{
		window.location.href = location;
	}
}

function ask() {
	return confirm('Are you sure..?');
};

function setFocus(element) {
	if(element !== undefined)
	{
		$(element).focus();
	}
};

function datePicker($element, $params) {
	if(typeof($element) === 'string')
	{
		$element = $($element);
	}

	$params = $.extend(true, {
		dateFormat: "yy-mm-dd",
		maxDate: 7,
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true,
		beforeShow: function(input) {
			setTimeout(function() {
				$("<button>", {
					class: "ui-datepicker-clear ui-state-default ui-priority-primary ui-corner-all",
					text: "Clear",
					click: function() {
						$.datepicker._clearDate(input);
					}
				}).appendTo($(input).datepicker("widget").find(".ui-datepicker-buttonpane"));
			}, 1);
		},
		onChangeMonthYear: function(year, month, instance) {
			setTimeout(function() {
				$("<button>", {
					class: "ui-datepicker-clear ui-state-default ui-priority-primary ui-corner-all",
					text: "Clear",
					click: function() {
						$.datepicker._clearDate(instance.input);
					}
				}).appendTo($(instance).datepicker("widget").find(".ui-datepicker-buttonpane"));
			}, 1);
		}
	}, $params);

	return $element.datepicker($params);
};

function timePicker($element, $params) {
	if(typeof($element) === 'string')
	{
		$element = $($element);
	}

	$params = $.extend(true, {
		shortTime: true,
		format: "YYYY-MM-DD HH:mm",
		closeText: '<i class="fa fa-times"></i>',
		nowClass: "buttons mini icon-left blue",
		nowText: '<i class="fa fa-hand-o-down"></i> Now',

		cancelClass: "buttons mini icon-left lightRed",
		cancelText: '<i class="fa fa-times"></i> Cancel',

		okClass: "buttons mini icon-left green",
		okText: '<i class="fa fa-check"></i> OK',

		arrowLeftText: '<i class="fa fa-caret-left"></i>',
		arrowLeftClass: "text-white",
		arrowRightText: '<i class="fa fa-caret-right"></i>',
		arrowRightClass: "text-white",

		arrowTopText: '<i class="fa fa-caret-up"></i>',
		arrowTopClass: "text-green",
		arrowBottomText: '<i class="fa fa-caret-down"></i>',
		arrowBottomClass: "text-green",
	}, $params);

	$element.bootstrapMaterialDatePicker("destroy").unbind().removeData();

	return $element.bootstrapMaterialDatePicker($params);
};

function callAjax(element, id, params) {
	if(element === undefined)
	{
		return false;
	}

	var $this = $(element);
	var $data = $this.data("ajax") || {};
	var $url = ($data.url) ? $data.url : $this.attr("href");
	var $target = $($data.target);
	var $params = {
		callBacks: {
			completeAfter: function() {}
		}
	};

	if(id !== false) $url += '/' + $this.val();
	if(params !== undefined)
	{
		$.extend(true, $params, params);

		if(params.data !== undefined)
		{
			$.each(params.data, function(key, value) {
				$data[key] = value;
			});
		}
	}

	return $.ajax($.extend(true, {
		type: "POST",
		url: $url,
		data: $data,
		cache: false,
		beforeSend: function() {}
	}, $params)).done(function(response) {

	}).fail(function(response) {

	}).always(function(response) {
		if($data.closest !== undefined)
		{
			$this.closest($data.closest).find($target).html(response);
		}
		else
		{
			$target.html(response);
		}

		$params.callBacks.completeAfter();
	});
};

function ajaxSubmit(element, params)
{
	var form = $(element);
	var $params = {
		callBack: {
			beforeSend: function() {},
			complete: function() {}
		}
	};

	if(params !== undefined) $.extend(true, $params, params);

	$.ajax({
		type: 'POST',
		url: form.attr('action'),
		data: form.serialize(),
		cache: false,
		beforeSend: function() {
			$params.callBack.beforeSend();
		},
		complete: function(response) {
			$params.callBack.complete(response.responseJSON);
		}
	});

	return false;
};